$(document).ready(function () {
    /* Configuration */
    var DEG = 'c', // c for celsius, f for fahrenheit
        weatherDiv = $('.weather'),
        API = '&APPID=9a43fdb9676f3bf22d3af52a55180aa9',
        warm =
        'url(http://www.mrwallpaper.com/wallpapers/sunny-day-landscape.jpg)',
        cold =
        'url(http://desktopwallpaperswide.com/wp-content/uploads/2012/11/winter-desktop-backgrounds-2.jpg)',
        UNITS = '&units=metric';


    if (navigator.geolocation) {
        // if browser supports geolocation
        navigator.geolocation.getCurrentPosition(locationFound,
            locationError);
    } else {
        $('.title').append("browser doesn't support location");
    }

    function locationFound(position) {
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;
        $.getJSON(
            'http://api.openweathermap.org/data/2.5/weather?lat=' +
            lat + '&lon=' + lon + API + UNITS,
            function (data) {
                $(".temp-max").text(data.main.temp_max);
                $(".temp-min").text(data.main.temp_min);
                $(".temp-current").text(data.main.temp);
                $(".humidity").text(data.main.humidity);
                $(".city").text(data.name);
                // $(".main").show();
                weatherDiv.fadeIn(1200);
                changeBackgrund(data.main.temp);
            });
    }

    function changeBackgrund(temprature) {
        console.log(temprature);
        if (Number(temprature) < 20) {
            $('body').css("background-image", warm);
        } else {
            $('body').css("background-image", cold);
        }

    }

    function locationError() {
        $('.title').text('Your Browser Doesn\'t support geolocation');
        weatherDiv.fadeIn(200);
    }
});
